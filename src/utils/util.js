/*
 * @Description: 工具函数
 * @Author: ls
 * @Date: 2019-09-04 09:10:30
 * @LastEditTime: 2019-09-24 16:52:58
 * @LastEditors: Please set LastEditors
 */
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


module.exports = {
  formatTime: formatTime
}
